<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/07/16
 * Time: 14:20
 */

namespace eezeecommerce\StockBundle\Tests\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use eezeecommerce\StockBundle\Entity\Stock;
use eezeecommerce\StockBundle\Entity\Transactions;

class StockTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Stock
     */
    private $stock;

    public function setUp()
    {
        $this->stock = new Stock();
    }

    public function testEntityWillReturnActualStock()
    {
        $this->assertEquals(0, $this->stock->getCurrentStock());

        $this->stock->setStock(19);

        $this->assertEquals(19, $this->stock->getCurrentStock());
    }

    public function testEntityWithTransactionWillReturnCorrectValue()
    {
        $transaction = $this->getMockBuilder(Transactions::class)
            ->disableOriginalConstructor()
            ->getMock();

        $transaction->expects($this->any())
            ->method("getAmount")
            ->will($this->returnValue(-1));

        $this->stock->addTransaction($transaction);

        $this->stock->setStock(10);

        $this->assertEquals(9, $this->stock->getCurrentStock());

        $transaction2 = $this->getMockBuilder(Transactions::class)
            ->disableOriginalConstructor()
            ->getMock();

        $transaction2->expects($this->once())
            ->method("getAmount")
            ->will($this->returnValue(-76));

        $this->stock->addTransaction($transaction2);

        $this->assertEquals(-67, $this->stock->getCurrentStock());
    }
}