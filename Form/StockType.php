<?php

namespace eezeecommerce\StockBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StockType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('stock', null, array(
                    "label" => false,))
                ->add('stop_backorder', null, array(
                    "label" => "Stop customers from being able to backorder this item? (stop orders when out-of-stock)"
                ))
                ->add('override_out_of_stock', null, array(
                    "label" => "Set this item to out-of-stock (stops orders)"
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\StockBundle\Entity\Stock'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_stockbundle_stock';
    }

}
