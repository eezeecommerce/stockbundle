<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/07/16
 * Time: 16:33
 */

namespace eezeecommerce\StockBundle\Manager;


use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\StockBundle\Entity\Stock;
use eezeecommerce\StockBundle\Entity\Transactions;
use Doctrine\Common\Persistence\ObjectManager;

class StockManager
{
    /**
     * Doctrine Object Manager Instance
     *
     * @var ObjectManager
     */
    private $em;

    /**
     * Stock Entity
     *
     * @var Stock
     */
    private $stock;

    /**
     * Amount to be Added or Subtracted
     *
     * @var integer
     */
    private $value;

    /**
     * StockManager constructor.
     * @param ObjectManager $em Doctrine Object Manager Instance
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * Set Product code and amount to be added/subtracted
     *
     * @param Product $product Product Entity instance
     * @param Integer $value   amount to add/subtract
     *
     * @return void
     */
    public function setProduct(Stock $stock, $value)
    {
        $this->stock = $stock;

        if (!is_int($value)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Value should be an integer instead %s was passed through",
                    gettype($value)
                )
            );
        }

        $this->value = $value;
    }

    /**
     * Persist Transaction to stock when item has been ordered
     *      if product doesn't contain a stock entity then it will
     *      return null
     * @return void
     */
    public function persist()
    {
        if (null === $this->value) {
            throw new \InvalidArgumentException(
                "Please call setProduct before calling persist"
            );
        }

        if (null === $this->stock) {
            return;
        }

        $transaction = new Transactions();
        $transaction->setStock($this->stock);
        $transaction->setAmount($this->value);

        $this->em->persist($transaction);
        $this->em->flush();
    }
}