<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */
namespace eezeecommerce\StockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Transactions
 *
 * @author root
 */

/**
 * @ORM\Entity(repositoryClass="eezeecommerce\StockBundle\Entity\TransactionsRepository")
 * @ORM\Table(name="transactions")
 */
class Transactions
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\StockBundle\Entity\Stock", inversedBy="transactions")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amount;
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $ts;

    public function __construct()
    {
        $this->setTs(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Transactions
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set ts
     *
     * @param \DateTime $ts
     *
     * @return Transactions
     */
    public function setTs(\DateTime $ts)
    {
        $this->ts = $ts;

        return $this;
    }

    /**
     * Get ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set stock
     *
     * @param \eezeecommerce\StockBundle\Entity\Stock $stock
     *
     * @return Transactions
     */
    public function setStock(\eezeecommerce\StockBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \eezeecommerce\StockBundle\Entity\Stock
     */
    public function getStock()
    {
        return $this->stock;
    }
}
