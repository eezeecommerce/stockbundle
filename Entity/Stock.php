<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */
namespace eezeecommerce\StockBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Stock
 *
 * @author root
 */

/**
 * @ORM\Entity(repositoryClass="eezeecommerce\StockBundle\Entity\StockRepository")
 * @ORM\Table(name="stock")
 */
class Stock
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Product", inversedBy="stock")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $product;
    
    /**
     * @ORM\OneToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Variants", inversedBy="stock")
     * @ORM\JoinColumn(name="variant_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $variant;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $stock = 0;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $override_out_of_stock = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $stop_backorder = false;
    
    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\StockBundle\Entity\Transactions", mappedBy="stock", cascade={"persist"})
     */
    protected $transactions;
    
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Stock
     */
    public function setStock($stock = 0)
    {
        if (null === $stock) {
            $stock = 0;
        }
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set overrideOutOfStock
     *
     * @param boolean $overrideOutOfStock
     *
     * @return Stock
     */
    public function setOverrideOutOfStock($overrideOutOfStock)
    {
        $this->override_out_of_stock = $overrideOutOfStock;

        return $this;
    }

    /**
     * Get overrideOutOfStock
     *
     * @return boolean
     */
    public function getOverrideOutOfStock()
    {
        return $this->override_out_of_stock;
    }

    /**
     * Set product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return Stock
     */
    public function setProduct(\eezeecommerce\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \eezeecommerce\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set variant
     *
     * @param \eezeecommerce\ProductBundle\Entity\Variants $variant
     *
     * @return Stock
     */
    public function setVariant(\eezeecommerce\ProductBundle\Entity\Variants $variant = null)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return \eezeecommerce\ProductBundle\Entity\Variants
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * Add transaction
     *
     * @param \eezeecommerce\StockBundle\Entity\Transactions $transaction
     *
     * @return Stock
     */
    public function addTransaction(\eezeecommerce\StockBundle\Entity\Transactions $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \eezeecommerce\StockBundle\Entity\Transactions $transaction
     */
    public function removeTransaction(\eezeecommerce\StockBundle\Entity\Transactions $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Set stopBackorder
     *
     * @param boolean $stopBackorder
     *
     * @return Stock
     */
    public function setStopBackorder($stopBackorder)
    {
        $this->stop_backorder = $stopBackorder;

        return $this;
    }

    /**
     * Get stopBackorder
     *
     * @return boolean
     */
    public function getStopBackorder()
    {
        return $this->stop_backorder;
    }

    public function getCurrentStock()
    {
        $amount = array();

        foreach ($this->transactions as $transaction) {
            $amount[] = $transaction->getAmount();
        }

        $diff = array_sum($amount);

        return $this->stock + $diff;
    }

    public function setCurrentStock($val)
    {
        $currentStock = $this->getCurrentStock();

        $diff = ($val - $currentStock);

        $transaction = new Transactions();
        $transaction->setAmount($diff);
        $transaction->setStock($this);

        $this->addTransaction($transaction);

        return $this;
    }
}
