<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/07/16
 * Time: 14:34
 */

namespace eezeecommerce\StockBundle\EventSubscriber;


use eezeecommerce\CartBundle\CartEvents;
use eezeecommerce\CartBundle\Event\FilterItemEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class StockSubscriber implements EventSubscriberInterface
{
    /**
     * @var FlashBag
     */
    private $flashBag;

    /**
     * @var Request
     */
    private $request;

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            CartEvents::CART_ITEM_ADD_INITIALISE => array(
                "onCartAdd", 150
            ),
            CartEvents::CART_ITEM_UPDATE_INITIALISE => array(
                "onCartUpdate", 150
            )
        );
    }

    public function setFlashBag(FlashBag $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    public function setRequestStack(RequestStack $request)
    {
        $this->request = $request->getMasterRequest();
    }

    public function onCartAdd(FilterItemEvent $event)
    {
        $entity = $event->getItem()->getEntity();

        if (null === $entity) {
            return;
        }

        if (count($entity->getVariants()) > 0) {
            $entity = $event->getItem()->getChosenVariant();
        }

        $stock = $entity->getStock();

        if (null === $stock) {
            return;
        }

        if ($stock->getOverrideOutOfStock()) {
            $this->flashBag->set("danger", sprintf(
                    "Unfortunately, The item you are trying to order is out of stock"
                )
            );

            $response = new RedirectResponse($this->request->headers->get("referer"));

            return $event->setResponse($response);
        }

        $qty = $event->getQuantity();

        if (false === $stock->getStopBackorder()) {
            return;
        }

        if ($stock->getCurrentStock() >= $qty) {
            return;
        }

        if (null === $this->request) {
            return;
        }

        if (!$this->request->headers->has("referer")) {
            return;
        }

        $this->flashBag->set("danger", sprintf(
                "Unfortunately, We only have %s of %s in stock and %s was added to your cart",
                $stock->getCurrentStock(),
                $entity->getStockCode(),
                $qty
            )
        );

        $response = new RedirectResponse($this->request->headers->get("referer"));

        return $event->setResponse($response);
    }

    public function onCartUpdate(FilterItemEvent $event)
    {

        return $this->onCartAdd($event);
    }
}